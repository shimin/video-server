#!/bin/bash

if [ "$EUID" -ne 0 ]
	then echo "Must be root"
	exit
fi

apt-get update && sudo apt-get -y upgrade
apt-get remove --purge hostapd -y
apt-get -y install hostapd dnsmasq
apt-get -y install libpcre3 libpcre3-dev openssl libssl-dev zlib1g

APPASS="videopass"
APSSID="rspi3"
ROOT_PATH=$(cd $(dirname $0) && pwd);

cat > /etc/dnsmasq.conf <<EOF
interface=wlan0
dhcp-range=10.0.0.2,10.0.0.10,255.255.255.0,12h
EOF

cat > /etc/hostapd/hostapd.conf <<EOF
interface=wlan0
hw_mode=g
channel=10
auth_algs=1
wpa=2
wpa_key_mgmt=WPA-PSK
wpa_pairwise=CCMP
rsn_pairwise=CCMP
wpa_passphrase=$APPASS
ssid=$APSSID
EOF

sed -i -- 's/exit 0/ /g' /etc/rc.local

cat >> /etc/rc.local <<EOF
ifconfig wlan0 down
ifconfig wlan0 10.0.0.1 netmask 255.255.255.0 up
iwconfig wlan0 power off
service dnsmasq restart
service nginx restart
hostapd -B /etc/hostapd/hostapd.conf & > /dev/null 2>&1

exit 0

EOF

service apache2 stop
update-rc.d -f apache2 remove

cd ~
wget http://nginx.org/download/nginx-1.9.15.tar.gz
wget https://github.com/arut/nginx-rtmp-module/archive/master.zip
tar -zxvf nginx-1.9.15.tar.gz
unzip master.zip
cd nginx-1.9.15
./configure --prefix=/usr --conf-path=/etc/nginx/nginx.conf --pid-path=/var/run/nginx.pid --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --with-http_ssl_module --add-module=../nginx-rtmp-module-master
make
sudo make install

cat >> /lib/systemd/system/nginx.service <<EOF
[Unit]
Description=The NGINX HTTP and reverse proxy server
After=syslog.target network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
PIDFile=/var/run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t
ExecStart=/usr/sbin/nginx
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s QUIT $MAINPID
PrivateTmp=true

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload

cd ~
wget https://github.com/ccrisan/motioneye/wiki/precompiled/ffmpeg_3.1.1-1_armhf.deb
dpkg -i ffmpeg_3.1.1-1_armhf.deb

mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.old
cp $ROOT_PATH/nginx.conf /etc/nginx/
rm /usr/html/index.html
cp $ROOT_PATH/index.html /usr/html/
cp $ROOT_PATH/ios.html /usr/html/

mkdir -p /var/www/cache/local/tmp/hls-1/
ln -s /var/www/cache/local/tmp/hls-1/ /usr/html/hls
cd /usr/html/
git clone https://github.com/dailymotion/hls.js.git

#update-rc.d nginx enable
service nginx start

echo "Setup completed."
