### Функции скрипта: ###
* Настраивает rspi3 как dhcp сервер используя встроенный wifi модуль ssid=rspi3 password=videopass
* Устанавливает nginx сервер с модулем rtsp
* Настраивает nginx сервер на для получения потока с камеры в подсети 10.0.0.x и перекодирования в HLS по адресу /hls/live.m3u8
* Создает html страницу с видео-тегами для просмотра потока

### Инструкция по запуску ###

* Перед запуском обязательно расширить файловую систему: raspi-config --expand-rootfs
* Клонировать в пользовательскую директорию
* Дать права доступа на исполнение: chmod +x ./rspi3.sh
* Запускать от имени su: sudo ./rspi3.sh &>./script.log
* Перезагрузиться.
* Адрес выданный камере можно увидеть в файле: /var/lib/misc/dnsmasq.leases (устройство имеет сетевое имя "*")
* Чтобы изменить адрес камеры и адрес выдачи потока, нужно исправить /etc/nginx/nginx.conf и html файл.
* Любые изменения в конфигурации применяются после перезапуска nginx: sudo service nginx restart
* В выводе необходимо уточнить, что дополнительный модуль подключился:

```
#!bash

configuring additional modules
adding module in ../nginx-rtmp-module-master/
 + ngx_rtmp_module was configured
checking for PCRE library ... found

```


### Описание файлов ###

* nginx.conf - определяет общие настройки nginx а так же rtmp модуля. Находится в /etc/nginx/
* index.html - html сраница по умолчанию, содержит контент для Android, десктопных устройств
* ios.html - html страница с контентом для ios устройств.

Проверено на 2016-03-18-raspbian-jessie

